import { ErrorMapper } from "utils/ErrorMapper";
import { BuilderRole } from "./roles/builder.role";
import { CreepLevel, RoleType } from "./roles/creep.role";
import { HarvesterRole } from "./roles/harvester.role";
import { RepairerRole } from "./roles/repairer.role";
import { UpgraderRole } from "./roles/upgrader.role";
import { CreepMemory } from "./types";

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
const harvesterRole = new HarvesterRole();
const builderRole = new BuilderRole();
const repairerRole = new RepairerRole();
const upgraderRole = new UpgraderRole();
const roleMap = {
    [RoleType.HARVESTER]: harvesterRole,
    [RoleType.BUILDER]: builderRole,
    [RoleType.REPAIRER]: repairerRole,
    [RoleType.UPGRADER]: upgraderRole
};
const roleRequestCount = {
    [RoleType.HARVESTER]: 2,
    [RoleType.BUILDER]: 2,
    [RoleType.REPAIRER]: 1,
    [RoleType.UPGRADER]: 2
};
const spawnOrder = [RoleType.HARVESTER, RoleType.UPGRADER, RoleType.REPAIRER, RoleType.BUILDER];
const defaultSpawn = "Spawn1";
export const loop = ErrorMapper.wrapLoop(() => {
    // Automatically delete memory of missing creeps
    for (const name in Memory.creeps) {
        if (!(name in Game.creeps)) {
            delete Memory.creeps[name];
        }
    }
    repopulate();
    runCreeps();
});

function repopulate() {
    if (Object.keys(Game.creeps).length === 0) {
        harvesterRole.create(Game.spawns[defaultSpawn], CreepLevel.START);
    } else {
        const roleCounts: { [s: string]: number } = {};
        for (const creep of Object.values(Game.creeps)) {
            const memory = creep.memory as CreepMemory;
            roleCounts[memory.role] = (roleCounts[memory.role] || 0) + 1;
        }
        for (const roleType of spawnOrder) {
            if (roleRequestCount[roleType] > (roleCounts[roleType] || 0)) {
                roleMap[roleType].create(Game.spawns[defaultSpawn], CreepLevel.LARGEST_POSSIBLE);
                break;
            }
        }
    }
}

function runCreeps() {
    for (const creepName in Game.creeps) {
        const creep = Game.creeps[creepName];
        const memory = creep.memory as CreepMemory;
        roleMap[memory.role].run(creep);
    }
}
