// example declaration file - remove these and add your own custom typings

// memory extension samples
import { CreepLevel, RoleType } from "./roles/creep.role";

interface CreepMemory {
    role: RoleType;
    level: CreepLevel;
    working?: boolean;
}

interface Memory {
    uuid: number;
    log: any;
}

// `global` extension samples
declare namespace NodeJS {
    interface Global {
        log: any;
    }
}
