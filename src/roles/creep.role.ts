export enum CreepLevel {
    START = "start",
    SMALL = "small",
    MEDIUM = "medium",
    LARGE = "large",
    LARGEST_POSSIBLE = "largest_possible"
}

export abstract class CreepRole<T extends RoleType> {
    public create(spawner: StructureSpawn, level: CreepLevel): ScreepsReturnCode {
        if (level === CreepLevel.LARGEST_POSSIBLE) {
            let largestValue = 0;
            let largest: CreepLevel | undefined;
            for (const creepLevel of Object.values(CreepLevel) as Array<CreepLevel>) {
                const requirements = this.getLevelRequirements(creepLevel);
                if (
                    requirements > largestValue &&
                    spawner.spawnCreep(this.getBodyParts(creepLevel), "dummytestname", { dryRun: true }) === OK
                ) {
                    largestValue = requirements;
                    largest = creepLevel;
                }
            }
            if (largest && largest !== CreepLevel.LARGEST_POSSIBLE) {
                return this.create(spawner, largest);
            }
        }
        return spawner.spawnCreep(this.getBodyParts(level), this.generateName(), {
            memory: {
                role: this.getRole(),
                level
            }
        });
    }

    public getLevelRequirements(level: CreepLevel): number {
        let cost = 0;
        this.getBodyParts(level).forEach(part => (cost += BODYPART_COST[part]));
        return cost;
    }

    public abstract getPriority(): number;

    public abstract getRole(): T;

    public abstract getBodyParts(level: CreepLevel): Array<BodyPartConstant>;

    protected generateName(): string {
        return this.getRole() + Math.random().toString(36).substr(2, 7).toUpperCase();
    }

    public abstract run(creep: Creep): void;
}

export enum RoleType {
    BUILDER = "BUILDER",
    HARVESTER = "HARVESTER",
    REPAIRER = "REPAIRER",
    UPGRADER = "UPGRADER"
}
