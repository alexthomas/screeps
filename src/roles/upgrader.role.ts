import { CreepMemory } from "../types";
import { CreepLevel, CreepRole, RoleType } from "./creep.role";

export class UpgraderRole extends CreepRole<RoleType.UPGRADER> {
    public getPriority(): number {
        return 4;
    }

    public getRole(): RoleType.UPGRADER {
        return RoleType.UPGRADER;
    }

    public getBodyParts(level: CreepLevel): Array<BodyPartConstant> {
        switch (level) {
            case CreepLevel.START:
                return [MOVE, WORK, CARRY];
            case CreepLevel.SMALL:
                return [MOVE, WORK, WORK, CARRY];
            case CreepLevel.MEDIUM:
                return [MOVE, WORK, WORK, WORK, WORK, CARRY, CARRY];
            default:
                return [];
        }
    }

    public run(creep: Creep): void {
        const memory = creep.memory as CreepMemory;
        if (creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0 && !memory.working) {
            const closestSource = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
            if (closestSource) {
                if (creep.harvest(closestSource) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestSource);
                }
                if (creep.store.getFreeCapacity(RESOURCE_ENERGY) < 5) {
                    memory.working = true;
                }
            } else {
                creep.say("No source available");
            }
        } else {
            if (creep.store.getUsedCapacity(RESOURCE_ENERGY) === 0) {
                memory.working = false;
            }
            const closestController = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                filter: { structureType: STRUCTURE_CONTROLLER }
            }) as StructureController;
            if (closestController) {
                if (creep.upgradeController(closestController) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestController);
                }
            } else {
                creep.say("No Controller available");
            }
        }
    }
}
