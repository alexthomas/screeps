import { CreepLevel, CreepRole, RoleType } from "./creep.role";

export class HarvesterRole extends CreepRole<RoleType.HARVESTER> {
    private readonly DEPOSIT_ORDER = [STRUCTURE_CONTAINER, STRUCTURE_EXTENSION, STRUCTURE_SPAWN];

    public getPriority(): number {
        return 0;
    }

    public getRole(): RoleType.HARVESTER {
        return RoleType.HARVESTER;
    }

    public getBodyParts(level: CreepLevel): Array<BodyPartConstant> {
        switch (level) {
            case CreepLevel.START:
                return [MOVE, WORK, CARRY];
            case CreepLevel.SMALL:
                return [MOVE, WORK, CARRY, CARRY];
            case CreepLevel.MEDIUM:
                return [MOVE, WORK, WORK, WORK, CARRY, CARRY, CARRY];
            default:
                return [];
        }
    }

    public run(creep: Creep): void {
        if (creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
            const closestSource = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
            if (closestSource) {
                if (creep.harvest(closestSource) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestSource);
                }
            } else {
                creep.say("No source available");
            }
        } else {
            const closestDeposit = this.findClosestDepositLocation(creep);
            if (closestDeposit) {
                if (creep.transfer(closestDeposit, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestDeposit);
                }
            } else {
                creep.say("No Spawn available");
            }
        }
    }

    private findClosestDepositLocation(creep: Creep): StructureContainer | StructureExtension | StructureSpawn | null {
        for (const structureType of this.DEPOSIT_ORDER) {
            const closest = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (object: Structure & { store: StoreDefinition }) =>
                    object.structureType === structureType && object.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            }) as StructureContainer | StructureExtension | StructureSpawn | null;
            if (closest) {
                return closest;
            }
        }
        return null;
    }
}
