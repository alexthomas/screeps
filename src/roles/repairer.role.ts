import { CreepMemory } from "../types";
import { CreepLevel, CreepRole, RoleType } from "./creep.role";

export class RepairerRole extends CreepRole<RoleType.REPAIRER> {
    private readonly ENERGY_SOURCES: Array<StructureConstant> = [STRUCTURE_CONTAINER, STRUCTURE_SPAWN];
    protected readonly REPAIR_ORDER: Array<StructureConstant> = [STRUCTURE_CONTAINER, STRUCTURE_ROAD,STRUCTURE_EXTENSION];

    public getBodyParts(level: CreepLevel): Array<BodyPartConstant> {
        switch (level) {
            case CreepLevel.START:
                return [MOVE, WORK, CARRY];
            case CreepLevel.SMALL:
                return [MOVE, MOVE, WORK, CARRY];
            default:
                return [];
        }
    }

    public getPriority(): number {
        return 1;
    }

    public getRole(): RoleType.REPAIRER {
        return RoleType.REPAIRER;
    }

    public run(creep: Creep): void {
        const memory = creep.memory as CreepMemory;
        if (creep.store.getUsedCapacity() === 0) {
            memory.working = false;
            const closestEnergySource = this.findClosestEnergySource(creep);
            if (closestEnergySource) {
                if (creep.withdraw(closestEnergySource, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestEnergySource);
                }
            } else {
                creep.say("No energy source available");
            }
        }
        if (memory.working) {
            const closestTask = this.findClosestTask(creep);
            if (closestTask) {
                if (creep.repair(closestTask) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestTask);
                }
            } else {
                creep.say("Nothing to repair available");
            }
        }

        if (creep.store.getUsedCapacity() > 0) {
            memory.working = true;
        }
    }

    private findClosestEnergySource(creep: Creep): Structure | null {
        return this.findClosest(creep, this.ENERGY_SOURCES, (object: { store: StoreDefinition }) => {
            return object.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
        });
    }

    private findClosestTask(creep: Creep): Structure | null {
        return this.findClosest(creep, this.REPAIR_ORDER, object => object.hits<object.hitsMax);
    }

    private findClosest(
        creep: Creep,
        structure: StructureConstant | Array<StructureConstant>,
        filter: (object: Structure & { store: StoreDefinition }) => boolean
    ): Structure | null {
        if (structure instanceof Array) {
            for (const structureElement of structure) {
                const closest = this.findClosest(creep, structureElement, filter);
                if (closest) {
                    return closest;
                }
            }
            return null;
        }
        return creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (object: Structure & { structureType: StructureConstant; store: StoreDefinition }) =>
                object.structureType === structure && filter(object)
        });
    }
}
