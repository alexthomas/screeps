import { CreepMemory } from "../types";
import { CreepLevel, CreepRole, RoleType } from "./creep.role";

export class BuilderRole extends CreepRole<RoleType.BUILDER> {
    private readonly ENERGY_SOURCES: Array<StructureConstant> = [STRUCTURE_CONTAINER, STRUCTURE_SPAWN];
    protected readonly BUILD_ORDER: Array<StructureConstant> = [
        STRUCTURE_CONTAINER,
        STRUCTURE_EXTENSION,
        STRUCTURE_ROAD
    ];

    public getBodyParts(level: CreepLevel): Array<BodyPartConstant> {
        switch (level) {
            case CreepLevel.START:
                return [MOVE, WORK, CARRY, CARRY];
            case CreepLevel.SMALL:
                return [MOVE, WORK, WORK, CARRY, CARRY];
            case CreepLevel.MEDIUM:
                return [MOVE, WORK, WORK, WORK, CARRY, CARRY, CARRY];
            default:
                return [];
        }
    }

    public getPriority(): number {
        return 1;
    }

    public getRole(): RoleType.BUILDER {
        return RoleType.BUILDER;
    }

    public run(creep: Creep): void {
        const memory = creep.memory as CreepMemory;
        if (creep.store.getUsedCapacity() === 0) {
            memory.working = false;
            const closestEnergySource = this.findClosestEnergySource(creep);
            if (closestEnergySource) {
                if (creep.withdraw(closestEnergySource, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestEnergySource);
                }
            } else {
                creep.say("No energy source available");
            }
        }
        if (memory.working) {
            const closestTask = this.findClosestTask(creep);
            if (closestTask) {
                if (creep.build(closestTask) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestTask);
                }
            } else {
                creep.say("Nothing to build available");
            }
        }

        if (creep.store.getUsedCapacity() > 0) {
            memory.working = true;
        }
    }

    private findClosestEnergySource(creep: Creep): Structure | null {
        return this.findClosest(
            creep,
            FIND_STRUCTURES,
            this.ENERGY_SOURCES,
            (object: { store: StoreDefinition }) => {
                return object.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
            }
        ) as Structure | null;
    }

    private findClosestTask(creep: Creep): ConstructionSite | null {
        return this.findClosest(
            creep,
            FIND_MY_CONSTRUCTION_SITES,
            this.BUILD_ORDER,
            () => true
        ) as ConstructionSite | null;
    }

    private findClosest(
        creep: Creep,
        find: FIND_STRUCTURES | FIND_MY_CONSTRUCTION_SITES,
        structure: StructureConstant | Array<StructureConstant>,
        filter: (object: Structure & { store: StoreDefinition }) => boolean
    ): ConstructionSite<BuildableStructureConstant> | Structure | null {
        if (structure instanceof Array) {
            for (const structureElement of structure) {
                const closest = this.findClosest(creep, find, structureElement, filter);
                if (closest) {
                    return closest;
                }
            }
            return null;
        }
        return creep.pos.findClosestByPath(find, {
            filter: (object: Structure & { structureType: StructureConstant; store: StoreDefinition }) =>
                object.structureType === structure && filter(object)
        });
    }
}
